# Changelog

## 7.0.4-3.2.0/R-2.2.1rc3-3be41d1-2009141542 (2020-09-14)

#### New Features

* EVG took the part evgseqr.iocsh of the supercycleEngine.
#### Fixes

* Update iocsh scripts and startup examples to match new macros in DB

Full set of changes: [`7.0.4-3.2.0/R-2.2.1rc3-5f12528-2009141252...7.0.4-3.2.0/R-2.2.1rc3-3be41d1-2009141542`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.4-3.2.0/R-2.2.1rc3-5f12528-2009141252...7.0.4-3.2.0/R-2.2.1rc3-3be41d1-2009141542)

## 7.0.4-3.2.0/R-2.2.1rc3-5f12528-2009141252 (2020-09-14)


Full set of changes: [`7.0.4-3.2.0/R-2.2.1rc3-4e4bf39-2009111313...7.0.4-3.2.0/R-2.2.1rc3-5f12528-2009141252`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.4-3.2.0/R-2.2.1rc3-4e4bf39-2009111313...7.0.4-3.2.0/R-2.2.1rc3-5f12528-2009141252)

## 7.0.4-3.2.0/R-2.2.1rc3-4e4bf39-2009111313 (2020-09-11)

#### Fixes

* macro fix in evrr.iocsh.

Full set of changes: [`7.0.4-3.2.0/R-2.2.1rc3-b7a4514-2009110858...7.0.4-3.2.0/R-2.2.1rc3-4e4bf39-2009111313`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.4-3.2.0/R-2.2.1rc3-b7a4514-2009110858...7.0.4-3.2.0/R-2.2.1rc3-4e4bf39-2009111313)

## 7.0.4-3.2.0/R-2.2.1rc3-b7a4514-2009110858 (2020-09-11)

#### New Features

* Persistent deployment with screen and systemd added.
* Parallel image update with screen added for EVM only.
* run and bach dep dirs + td-d2x layer dep added.
* TD-D1x run added.
* EVG and td-m deployment added.
* td-d11 deployment added.
* td-d12 deployment added.
* Pseudo deployment for TD-NCL.
* Pseudo deployment for TD-NCL.
* Few local debugging files added.
#### Fixes

* CLKA duty cycle correction from 20% to 50%.
* evrlbseq0r added for the new naming standard.
#### Refactorings

* Deployment adjustment.
* td-d1x, td-d2x and td-m files changed into functions.
#### Docs

* gitlab CODEOWNERS, ISSUE_TEMPLATE and PULL_REQUEST_TEMPLATE added.
#### Others

* Run dir removed, dep and img managers added to tools.

Full set of changes: [`7.0.4-3.2.0/2.2.1rc1-913bfb3-20200715T210150...7.0.4-3.2.0/R-2.2.1rc3-b7a4514-2009110858`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.4-3.2.0/2.2.1rc1-913bfb3-20200715T210150...7.0.4-3.2.0/R-2.2.1rc3-b7a4514-2009110858)

## 7.0.4-3.2.0/2.2.1rc1-913bfb3-20200715T210150 (2020-07-07)

#### New Features

* Few local debugging files added.

Full set of changes: [`2.2.1rc2...7.0.4-3.2.0/2.2.1rc1-913bfb3-20200715T210150`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/2.2.1rc2...7.0.4-3.2.0/2.2.1rc1-913bfb3-20200715T210150)

## 2.2.1rc2 (2020-05-07)

#### New Features

* Optimization for the global environment access.
* Generic evr init file.
* Test evm added and PEP440 versioning.
#### Fixes

* evm.cmd changed to st-evm to comply to Conda.
#### Others

* st-evr deployment.

Full set of changes: [`2.2.1-rc.1, 2.2.1rc1...2.2.1rc2`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/2.2.1-rc.1, 2.2.1rc1...2.2.1rc2)

## 2.2.1-rc.1, 2.2.1rc1 (2020-04-14)

#### New Features

* 3U chassis added to mtca environment.
* evm.cmd added for the deployment inventory call.
* Generic evg.iocsh file added.
* Added evgasynr.iocsh
* evgr.iocsh added.
* mtca and ts environment added.
#### Fixes

* Fixed D macro
#### Refactorings

* Changelog adjustment for the automatic updates.
* ts.cmd master events moved to "reftabs".
* Updated iocsh/evm.iocsh with new macros
* No need for macro in embedded EVRs OBJ
* Changed SYS, D macros to P, R macros
* evm.iocsh for the ESS naming adjustment.
#### Docs

* Pull Request Template updated to follow the changelog standard.
* License definition added.
#### Others

* Lab tests.
* supercycleEngine integration tests.

Full set of changes: [`7.0.3.1-3.1.2/R-2.2.0-rc8-a1baa66-2002031236...2.2.1-rc.1, 2.2.1rc1`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.3.1-3.1.2/R-2.2.0-rc8-a1baa66-2002031236...2.2.1-rc.1, 2.2.1rc1)

## 7.0.3.1-3.1.2/R-2.2.0-rc8-a1baa66-2002031236 (2020-02-03)


Full set of changes: [`7.0.3.1-3.1.2/R-2.2.0-rc7-aa11391-1911121618...7.0.3.1-3.1.2/R-2.2.0-rc8-a1baa66-2002031236`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.3.1-3.1.2/R-2.2.0-rc7-aa11391-1911121618...7.0.3.1-3.1.2/R-2.2.0-rc8-a1baa66-2002031236)

## 7.0.3.1-3.1.2/R-2.2.0-rc7-aa11391-1911121618 (2019-11-11)


Full set of changes: [`7.0.3-3.1.2/R-2.2.0-rc7-2f412fa-1910251151...7.0.3.1-3.1.2/R-2.2.0-rc7-aa11391-1911121618`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.3-3.1.2/R-2.2.0-rc7-2f412fa-1910251151...7.0.3.1-3.1.2/R-2.2.0-rc7-aa11391-1911121618)

## 7.0.3-3.1.2/R-2.2.0-rc7-2f412fa-1910251151 (2019-10-21)


Full set of changes: [`7.0.3-3.1.1/R-2.2.0-rc6-c2dc739-1909271722...7.0.3-3.1.2/R-2.2.0-rc7-2f412fa-1910251151`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/7.0.3-3.1.1/R-2.2.0-rc6-c2dc739-1909271722...7.0.3-3.1.2/R-2.2.0-rc7-2f412fa-1910251151)

## 7.0.3-3.1.1/R-2.2.0-rc6-c2dc739-1909271722 (2019-09-27)


Full set of changes: [`2.2.0-rc5...7.0.3-3.1.1/R-2.2.0-rc6-c2dc739-1909271722`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/2.2.0-rc5...7.0.3-3.1.1/R-2.2.0-rc6-c2dc739-1909271722)

## 2.2.0-rc5 (2019-05-13)


Full set of changes: [`3.15.5-3.0.2/2.2.0-rc4-01923be-1811101812...2.2.0-rc5`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/3.15.5-3.0.2/2.2.0-rc4-01923be-1811101812...2.2.0-rc5)

## 3.15.5-3.0.2/2.2.0-rc4-01923be-1811101812 (2018-11-09)


Full set of changes: [`3.15.5-3.0.2/2.2.0-rc2-1aee5fb-1811091351...3.15.5-3.0.2/2.2.0-rc4-01923be-1811101812`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/3.15.5-3.0.2/2.2.0-rc2-1aee5fb-1811091351...3.15.5-3.0.2/2.2.0-rc4-01923be-1811101812)

## 3.15.5-3.0.2/2.2.0-rc2-1aee5fb-1811091351 (2018-11-05)


Full set of changes: [`3.15.5-3.0.0/2.2.0-rc2-1811020226...3.15.5-3.0.2/2.2.0-rc2-1aee5fb-1811091351`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/3.15.5-3.0.0/2.2.0-rc2-1811020226...3.15.5-3.0.2/2.2.0-rc2-1aee5fb-1811091351)

## 3.15.5-3.0.0/2.2.0-rc2-1811020226 (2018-10-06)


Full set of changes: [`R0.2.0.0...3.15.5-3.0.0/2.2.0-rc2-1811020226`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/R0.2.0.0...3.15.5-3.0.0/2.2.0-rc2-1811020226)

## R0.2.0.0 (2018-07-31)


Full set of changes: [`R0.1.0.0...R0.2.0.0`](https://gitlab.esss.lu.se/e3/timing/e3-mrfioc2/compare/R0.1.0.0...R0.2.0.0)

## R0.1.0.0 (2018-03-21)

