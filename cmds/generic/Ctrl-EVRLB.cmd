#!
epicsEnvSet "TOP" "$(E3_CMD_TOP)"
iocshLoad "$(TOP)/locenvs.iocsh"

epicsEnvSet "ENGINEER" "ICS_HWI_WP04"
epicsEnvSet "EPICS_CA_MAX_ARRAY_BYTES" "10000000"

iocshLoad "$(mrfioc2_DIR)/evr.iocsh" "P=$(PEVR), PCIID=$(PCI_SLOT), $(INITARGS=)"
iocshLoad "$(autosave_DIR)/autosave.iocsh"  "AS_TOP=$(AS_TOP=/var/log/autosave), IOCNAME=$(PEVR)"

iocInit

iocshLoad "$(TOP)/evrr.iocsh"                   "P=$(PEVR), INTREF=$(INTREF=)"
iocshLoad "$(TOP)/evrtclkr.iocsh"               "P=$(PEVR)"
iocshLoad "$(TOP)/evrlbseq0r.iocsh"             "P=$(PEVR)"
